angular.module('starter.controllers', [])

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
.controller('meusAnimaisCtrl', function($scope, meusAnimaisFac) {
  $scope.listaAnimaisDisponiveis = [];
  $scope.listaAnimaisAdotados = [];

  $scope.$on('$ionicView.enter', function(e) {
    meusAnimaisFac.getListaAnimaisDisponiveis().then(function(res){
      $scope.listaAnimaisDisponiveis = res;
    });

    meusAnimaisFac.getListaAnimaisAdotados().then(function(res){
      $scope.listaAnimaisAdotados = res;
    });
  });
})

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
.controller('detalheAnimalCtrl', function($scope, $stateParams, meusAnimaisFac, encontradosFac) {
  $scope.i_encontrado = $stateParams.i_encontrado;
  $scope.animal = {};
  $scope.coment = {};
  $scope.listaComentarios = [];

  $scope.$on('$ionicView.enter', function(e) {
    meusAnimaisFac.getInformacoesAnimal($scope.i_encontrado).then(function(res){
      $scope.animal = res;
    });

    encontradosFac.getListaComentarios($scope.i_encontrado).then(function(res) {
      $scope.listaComentarios = res;
    });
  });  

  $scope.fuiAdotado = function() {
    meusAnimaisFac.fuiAdotado($scope.i_encontrado).then(function(res){
      location.href = "#/tab/meus-animais";
    });
  };

  $scope.comentar = function() {
    $scope.coment.i_encontrado = $scope.i_encontrado;
    encontradosFac.setComentario($scope.coment).then(function(res) {
      encontradosFac.getListaComentarios($scope.i_encontrado).then(function(res) {
        $scope.listaComentarios = res;
      });
      $scope.coment = {};
    });
  }
})

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
.controller('animalEncontradoCtrl', function($scope, animaisFac, portesFac, coresFac, encontradosFac, $http) {
  $scope.$on('$ionicView.enter', function(e) {
    $scope.encontrado = {};
  });

  $scope.analisandoimg = false;
  var me = this;
  me.image_description = '';

  var api_key = 'AIzaSyCy14NakGmekKva2Af2B3w5hkXovZI9hJA';
 
  $scope.animal =  {single: null, multiple: null};
  $scope.porte =  {single: null, multiple: null};
  $scope.cor =  {single: null, multiple: null};
  $scope.sexo =  {single: null, multiple: null};

  animaisFac.getListaAnimais().then(function(res){
    $scope.listaAnimais = res;
  });

  portesFac.getListaPortes().then(function(res){
    $scope.listaPortes = res;
  });
  
  coresFac.getListaCores().then(function(res){
    $scope.listaCores = res;
  });

  $scope.listaSexo = [
    {id: 'F', texto: 'Fêmea'},
    {id: 'M', texto: 'Macho'}
  ];

  $scope.salvarEncontrado = function() {
    var latitude = 0;
    var longitude = 0;

    $scope.encontrado.filhote = $scope.encontrado.filhote_check?'S':'N';
    $scope.encontrado.i_animal = $scope.animal.single;
    $scope.encontrado.i_porte = $scope.porte.single;
    $scope.encontrado.i_cor = $scope.cor.single;
    $scope.encontrado.sexo = $scope.sexo.single;
    $scope.encontrado.status = 'E';
    $scope.encontrado.latitude = latitude.toString();
    $scope.encontrado.longitude = longitude.toString();

    encontradosFac.salvarEncontrado($scope.encontrado).then(function(res){
      location.href = '#/tab/meus-animais';
    });
  }

  $scope.trocarFoto = function() {
    var image_64 = $scope.encontrado.foto.replace('data:image/jpeg;base64,', '').replace('data:image/png;base64,', '').replace('data:image/jpg;base64,', '').replace('data:image/bmp;base64,', '');

    me.current_image = image_64;
    me.image_description = '';
    me.locale = '';

    var vision_api_json = {
      "requests":[
        {
          "image":{
            "content": me.current_image
          },
          "features":[
            {
              "type" : "SAFE_SEARCH_DETECTION",
              "maxResults": 10
            },
            {
              "type" : "LABEL_DETECTION",
              "maxResults" : 10
            }
          ]
        }
      ]
    };

    var url = 'https://vision.googleapis.com/v1/images:annotate?key='+api_key;
    var file_contents = JSON.stringify(vision_api_json);

    $scope.analisandoimg = true;
    $.ajax({
      url:url,
      type:"POST",
      data:file_contents,
      contentType:"application/json",
      dataType:"json",
      success: function(retorno){
        if (retorno) {
          var safe = retorno.responses[0].safeSearchAnnotation;
          var labelAnot = retorno.responses[0].labelAnnotations;

          //Verifica se a imagem é 'safe'
          if (safe.adult.indexOf("UNLIKELY") < 0 ||
              safe.medical.indexOf("UNLIKELY") < 0 ||
              safe.violence.indexOf("UNLIKELY") < 0) {
            $scope.encontrado.foto = null;
            $("#fotolouca").addClass('no-picture');  
            $("#fotolouca").css({
              'background-image': 'none'
            });
            alert('Detectamos que a imagem que você enviou pode conter conteúdo adulto ou violência, por favor envie uma nova imagem.');
            $scope.analisandoimg = false;
            $scope.$apply();
          } else {
            angular.forEach(labelAnot, function(value, key) {
              if (value.description.indexOf("kitten") >=0 || value.description.indexOf("puppy")>=0) {
                $scope.encontrado.filhote_check = true;
              }
            });

            $scope.analisandoimg = false;
            $scope.$apply();
          }
        }
      }
    });
  }
})

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
.controller('disponiveisCtrl', function($scope, encontradosFac) {
  $scope.$on('$ionicView.enter', function(e) {
    $scope.listaAnimais = [];
    $scope.listaComentarios = [];
    $scope.coment = {};
    $scope.index_lista = 0;

    encontradosFac.getListaEncontrados().then(function(res){
      $scope.listaAnimais = res;
      if ($scope.listaAnimais[$scope.index_lista]) {
        $scope.getListaComentarios($scope.listaAnimais[$scope.index_lista].i_encontrado);
      }
    });
  });

  $scope.proximo = function() {
    if ($scope.index_lista==$scope.listaAnimais.length-1) {
      $scope.index_lista = 0;
    } else {
      $scope.index_lista++;
    }

    $scope.getListaComentarios($scope.listaAnimais[$scope.index_lista].i_encontrado);
  }

  $scope.anterior = function() {
    if ($scope.index_lista==0) {
      $scope.index_lista = $scope.listaAnimais.length-1;
    } else {
      $scope.index_lista--; 
    }

    $scope.getListaComentarios($scope.listaAnimais[$scope.index_lista].i_encontrado);
  }

  $scope.like = function(i_encontrado) {
    $scope.listaAnimais[$scope.index_lista].liked = 'S';
    encontradosFac.like(i_encontrado).then(function(res) {
      $scope.listaAnimais[$scope.index_lista].qtd_likes = parseInt($scope.listaAnimais[$scope.index_lista].qtd_likes) + 1;
    });
  }

  $scope.dislike = function(i_encontrado) {
    $scope.listaAnimais[$scope.index_lista].liked = 'N';
    encontradosFac.dislike(i_encontrado).then(function(res) {
      $scope.listaAnimais[$scope.index_lista].qtd_likes = parseInt($scope.listaAnimais[$scope.index_lista].qtd_likes) - 1;
    });
  }

  $scope.adotar = function(i_encontrado) {
    if (confirm('Tem certeza que deseja registrar interesse neste animal?')) {
      encontradosFac.adotar(i_encontrado).then(function(res) {
        alert('Enviamos um e-mail ao responsável deste animal, em beve ele entrará em contato.');
      });
    }
  }

  $scope.getListaComentarios = function(i_encontrado) {
    encontradosFac.getListaComentarios(i_encontrado).then(function(res) {
      $scope.listaComentarios = res;
    });
  }

  $scope.comentar = function(i_encontrado) {
    $scope.coment.i_encontrado = i_encontrado;
    encontradosFac.setComentario($scope.coment).then(function(res) {
      $scope.getListaComentarios(i_encontrado);
      $scope.coment = {};
    });
  }
})

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
.controller('loginCtrl', function($scope, AuthenticationService, $localStorage) {
  $scope.$on('$ionicView.enter', function(e) {
    if ($localStorage.globals) {
      if ($localStorage.globals.currentUser.authdata) {
        location.href = "#/tab/meus-animais";
      }
    }
  });

  $scope.doLogin = function(login) {
    AuthenticationService.Login(login.username, login.password, function(response) {
      if(response.success) {
        AuthenticationService.SetCredentials(login.username, response.i_usuario);
        $scope.login = {};
      } else {
        $scope.error = response.message;
      }
    });

  };
})

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
.controller('perfilCtrl', function($scope, AuthenticationService) {
  $scope.logout = function(){
    if (confirm('Tem certeza que deseja sair?')){
      AuthenticationService.ClearCredentials(true);
    }
  }
})

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
.controller('criarUsuarioCtrl', function($scope, usuariosFac) {
  $scope.$on('$ionicView.enter', function(e) {
    $scope.lista_sexo = [
      {id: 'F', texto: 'Feminino'}, 
      {id: 'M', texto: 'Masculino'}
    ];
    
    $scope.sexo =  {single: null, multiple: null};

    $scope.usuario = {
      email : '',
      nome : '',
      sexo : '',
      senha: '',
      status : 'A'
    };

    $scope.senha = {confirmar_senha : ''};
  });

  $scope.salvarUsuario = function() {
    usuariosFac.verificaUsuario($scope.usuario).then(function(res){
      if (res.existe>0) {
        alert('Já existe um usuário cadastrado com este e-mail. Informe outro e-mail.')
        $scope.usuario.email = '';
      } else {
        if ($scope.senha.confirmar_senha!==$scope.usuario.senha){
          alert('As senhas digitadas não conferem.');
          $scope.usuario.senha = '';
          $scope.senha.confirmar_senha = '';
        } else {
          $scope.usuario.sexo = $scope.sexo.single;  
          usuariosFac.setUsuario($scope.usuario).then(function(res){
            $scope.usuario = {};
            location.href = '#/login';
          });
        }
      }
    });
  }
})

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
.controller('tabsCtrl', function($scope, AuthenticationService) {
  $scope.logout = function(){
    if (confirm('Tem certeza que deseja sair?')){
      AuthenticationService.ClearCredentials(true);
    }
  }
});
