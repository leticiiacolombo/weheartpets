if (location.hostname == 'localhost') {
  var url_base = 'http://localhost/pet/index.php';
} else {
  var url_base = 'http://leticiacolombo.esy.es/index.php';
}

//var url_base = 'http://leticiacolombo.esy.es/index.php';

angular.module('starter.services', [])

.service('LoadingInterceptor', 
  ['$q', '$rootScope', '$log', 
  function($q, $rootScope, $log) {
      'use strict';

      return {
          request: function(config) {
              $rootScope.loading = true;
              return config;
          },
          requestError: function(rejection) {
              $rootScope.loading = false;
              $log.error('Request error:', rejection);
              return $q.reject(rejection);
          },
          response: function(response) {
              $rootScope.loading = false;
              return response;
          },
          responseError: function(rejection) {
              $rootScope.loading = false;
              $log.error('Response error:', rejection);
              return $q.reject(rejection);
          }
      };
  }])

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
.factory('encontradosFac', function($http, AuthenticationService) {
  return {
    salvarEncontrado : function(obj) {
      var promise = $http.post(url_base+'/encontrados/setEncontrado', obj)
      .then(function(res){
        return res.data;
      }, function(error){
        console.error(error);
        if (error.status==401) {
          AuthenticationService.ClearCredentials(true);
        }
      });
      return promise;
    },
    getListaEncontrados : function() {
      var promise = $http.get(url_base+'/encontrados/getListaEncontrados')
      .then(function(res){
        return res.data;
      }, function(error){
        console.error(error);
        if (error.status==401) {
          AuthenticationService.ClearCredentials(true);
        }
      });
      return promise;
    },
    like : function(i_encontrado) {
      var promise = $http.post(url_base+'/encontrados/like/'+i_encontrado)
      .then(function(res){
        return res.data;
      }, function(error){
        console.error(error);
        if (error.status==401) {
          AuthenticationService.ClearCredentials(true);
        }
      });
      return promise;
    },
    dislike : function(i_encontrado) {
      var promise = $http.post(url_base+'/encontrados/dislike/'+i_encontrado)
      .then(function(res){
        return res.data;
      }, function(error){
        console.error(error);
        if (error.status==401) {
          AuthenticationService.ClearCredentials(true);
        }
      });
      return promise;
    },
    adotar : function(i_encontrado) {
      var promise = $http.post(url_base+'/encontrados/registrarInteresse/'+i_encontrado)
      .then(function(res){
        return res.data;
      }, function(error){
        console.error(error);
        if (error.status==401) {
          AuthenticationService.ClearCredentials(true);
        }
      });
      return promise;
    },
    getListaComentarios : function(i_encontrado) {
      var promise = $http.get(url_base+'/encontrados/getComentarios/'+i_encontrado)
      .then(function(res){
        return res.data;
      }, function(error){
        console.error(error);
        if (error.status==401) {
          AuthenticationService.ClearCredentials(true);
        }
      });
      return promise;
    },
    setComentario : function(comentario) {
      var promise = $http.post(url_base+'/encontrados/postarComentario', comentario)
      .then(function(res){
        return res.data;
      }, function(error){
        console.error(error);
        if (error.status==401) {
          AuthenticationService.ClearCredentials(true);
        }
      });
      return promise;
    },
    verificarFoto : function(url, img) {
      var promise = $http.post(url, img)
      .then(function(res){
        return res;
      }, function(error){
        console.error(error);
      });
      return promise;
    }
  };
})

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
.factory('meusAnimaisFac', function($http, AuthenticationService){
  return {
    getListaAnimaisDisponiveis : function() {
      var promise = $http.get(url_base+'/encontrados/getListaMeusAnimais')
      .then(function(res){
        return res.data;
      }, function(error){
        console.error(error);
        if (error.status==401) {
          AuthenticationService.ClearCredentials(true);
        }
      });
      return promise;
    },
    getListaAnimaisAdotados : function() {
      var promise = $http.get(url_base+'/encontrados/getListaMeusAnimaisAdotados')
      .then(function(res){
        return res.data;
      }, function(error){
        console.error(error);
        if (error.status==401) {
          AuthenticationService.ClearCredentials(true);
        }
      });
      return promise;
    },
    fuiAdotado : function(i_encontrado) {
      var promise = $http.post(url_base+'/encontrados/fuiAdotado/'+i_encontrado)
      .then(function(res){
        return res.data;
      }, function(error){
        console.error(error);
        if (error.status==401) {
          AuthenticationService.ClearCredentials(true);
        }
      });
      return promise;
    },
    getInformacoesAnimal : function(i_encontrado) {
       var promise = $http.get(url_base+'/encontrados/getInformacoesAnimal/'+i_encontrado)
      .then(function(res){
        return res.data;
      }, function(error){
        console.error(error);
        if (error.status==401) {
          AuthenticationService.ClearCredentials(true);
        }
      });
      return promise;
    }
  };
})

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
.factory('animaisFac', function($http, AuthenticationService){
  return {
    getListaAnimais : function() {
      var promise = $http.get(url_base+'/animais/getListaAnimais')
      .then(function(res){
        return res.data;
      }, function(error){
        console.error(error);
        if (error.status==401) {
          AuthenticationService.ClearCredentials(true);
        }
      });
      return promise;
    }
  };
})

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
.factory('portesFac', function($http, AuthenticationService){
  return {
    getListaPortes : function() {
      var promise = $http.get(url_base+'/portes/getListaPortes')
      .then(function(res){
        return res.data;
      }, function(error){
        console.error(error);
        if (error.status==401) {
          AuthenticationService.ClearCredentials(true);
        }
      });
      return promise;
    }
  };
})

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
.factory('coresFac', function($http, AuthenticationService){
  return {
    getListaCores : function() {
      var promise = $http.get(url_base+'/cores/getListaCores')
      .then(function(res){
        return res.data;
      }, function(error){
        console.error(error);
        if (error.status==401) {
          AuthenticationService.ClearCredentials(true);
        }
      });
      return promise;
    }
  };
})

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
.factory('usuariosFac', function($http, AuthenticationService){
  return {
    setUsuario : function(obj) {
      var promise = $http.post(url_base+'/usuarios/setUsuario', obj)
      .then(function(res){
        return res.data;
      }, function(error){
        console.error(error);
        if (error.status==401) {
          AuthenticationService.ClearCredentials(true);
        }
      });
      return promise;
    },
    verificaUsuario : function(obj) {
      var promise = $http.get(url_base+'/usuarios/verificaExistenciaUsuario', obj)
      .then(function(res){
        return res.data;
      }, function(error){
        console.error(error);
        if (error.status==401) {
          AuthenticationService.ClearCredentials(true);
        }
      });
      return promise;
    }
  };
})

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
.factory('AuthenticationService', function (Base64, $http, $rootScope, $timeout, $localStorage) {
  var service = {};

  service.Login = function (username, password, callback) {
    $http.post(url_base+'/login/login', { username: username, password: password })
      .success(function (response) {
        callback(response);
      }).error(function(err){
        alert(err.mensagem)
      });
  };

  service.SetCredentials = function (username, i_usuario) {
    var authdata = '';
    $http.get(url_base+'/login/gerarToken/'+i_usuario).success(function(response){
      authdata = Base64.encode(response.token);

      $localStorage.globals = {
        currentUser: {
          username: username,
          authdata: authdata
        }
      };

      $http.defaults.headers.common.User = authdata;

      location.href = "#/tab/meus-animais";
    }).error(function(err){
      $localStorage.globals = {
        currentUser: null
      };
      $http.defaults.headers.common.User = '';
    });;
  };

  service.ClearCredentials = function (redirecionar) {
    $localStorage.globals = {
      currentUser: ""
    };
    $http.defaults.headers.common.User = '';

    if (redirecionar){
      location.href="#/login";
    }
  };

  return service;
})

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

.factory('Base64', function () {
    /* jshint ignore:start */
  
    var keyStr = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/=';
  
    return {
        encode: function (input) {
            var output = "";
            var chr1, chr2, chr3 = "";
            var enc1, enc2, enc3, enc4 = "";
            var i = 0;
  
            do {
                chr1 = input.charCodeAt(i++);
                chr2 = input.charCodeAt(i++);
                chr3 = input.charCodeAt(i++);
  
                enc1 = chr1 >> 2;
                enc2 = ((chr1 & 3) << 4) | (chr2 >> 4);
                enc3 = ((chr2 & 15) << 2) | (chr3 >> 6);
                enc4 = chr3 & 63;
  
                if (isNaN(chr2)) {
                    enc3 = enc4 = 64;
                } else if (isNaN(chr3)) {
                    enc4 = 64;
                }
  
                output = output +
                    keyStr.charAt(enc1) +
                    keyStr.charAt(enc2) +
                    keyStr.charAt(enc3) +
                    keyStr.charAt(enc4);
                chr1 = chr2 = chr3 = "";
                enc1 = enc2 = enc3 = enc4 = "";
            } while (i < input.length);
  
            return output;
        },
  
        decode: function (input) {
            var output = "";
            var chr1, chr2, chr3 = "";
            var enc1, enc2, enc3, enc4 = "";
            var i = 0;
  
            // remove all characters that are not A-Z, a-z, 0-9, +, /, or =
            var base64test = /[^A-Za-z0-9\+\/\=]/g;
            if (base64test.exec(input)) {
                window.alert("There were invalid base64 characters in the input text.\n" +
                    "Valid base64 characters are A-Z, a-z, 0-9, '+', '/',and '='\n" +
                    "Expect errors in decoding.");
            }
            input = input.replace(/[^A-Za-z0-9\+\/\=]/g, "");
  
            do {
                enc1 = keyStr.indexOf(input.charAt(i++));
                enc2 = keyStr.indexOf(input.charAt(i++));
                enc3 = keyStr.indexOf(input.charAt(i++));
                enc4 = keyStr.indexOf(input.charAt(i++));
  
                chr1 = (enc1 << 2) | (enc2 >> 4);
                chr2 = ((enc2 & 15) << 4) | (enc3 >> 2);
                chr3 = ((enc3 & 3) << 6) | enc4;
  
                output = output + String.fromCharCode(chr1);
  
                if (enc3 != 64) {
                    output = output + String.fromCharCode(chr2);
                }
                if (enc4 != 64) {
                    output = output + String.fromCharCode(chr3);
                }
  
                chr1 = chr2 = chr3 = "";
                enc1 = enc2 = enc3 = enc4 = "";
  
            } while (i < input.length);
  
            return output;
        }
    };
  
    /* jshint ignore:end */
});
