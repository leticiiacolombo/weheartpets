// Ionic Starter App

// angular.module is a global place for creating, registering and retrieving Angular modules
// 'starter' is the name of this angular module example (also set in a <body> attribute in index.html)
// the 2nd parameter is an array of 'requires'
// 'starter.services' is found in services.js
// 'starter.controllers' is found in controllers.js
angular.module('starter', ['ionic', 'starter.controllers', 'starter.services', 'ion-profile-picture', 'ngStorage'])

.run(function($ionicPlatform, $localStorage, $rootScope, $http, $location) {
  $ionicPlatform.ready(function() {
    // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
    // for form inputs)
    if (window.cordova && window.cordova.plugins && window.cordova.plugins.Keyboard) {
      cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
      cordova.plugins.Keyboard.disableScroll(true);

    }
    if (window.StatusBar) {
      // org.apache.cordova.statusbar required
      StatusBar.styleDefault();
    }

    if ($localStorage.globals) {
      $rootScope.globals = {currentUser : $localStorage.globals.currentUser};
    } else {
      $rootScope.globals = {currentUser : ""};
    }

    // keep user logged in after page refresh
    if ($rootScope.globals.currentUser != "") {
      $http.defaults.headers.common.User = $rootScope.globals.currentUser.authdata;
    }

    $rootScope.$on('$locationChangeStart', function (event, next, current) {
      // redirect to login page if not logged in
      if ($location.path() !== '/login' && !$rootScope.globals.currentUser) {
        location.href="#/login";
      }
    });
  });
})

.config(function($stateProvider, $urlRouterProvider, $httpProvider) {

  // Ionic uses AngularUI Router which uses the concept of states
  // Learn more here: https://github.com/angular-ui/ui-router
  // Set up the various states which the app can be in.
  // Each state's controller can be found in controllers.js
  $stateProvider

  // setup an abstract state for the tabs directive
  .state('tab', {
    url: '/tab',
    abstract: true,
    templateUrl: 'templates/tabs.html',
    controller: 'tabsCtrl'
  })

  // Each tab has its own nav history stack:

  .state('tab.meus-animais', {
    url: '/meus-animais',
    views: {
      'tab-meus-animais': {
        templateUrl: 'templates/tab-meus-animais.html',
        controller: 'meusAnimaisCtrl'
      }
    }
  })

  .state('tab.meus-animais-detalhes', {
    url: '/meus-animais/:i_encontrado',
    views: {
      'tab-meus-animais': {
        templateUrl: 'templates/tab-detalhe-animal.html',
        controller: 'detalheAnimalCtrl'
      }
    }
  })

  .state('tab.animal-encontrado', {
    url: '/animal-encontrado',
    views: {
      'tab-meus-animais': {
        templateUrl: 'templates/animal-encontrado.html',
        controller: 'animalEncontradoCtrl'
      }
    }
  })

  .state('tab.disponiveis', {
    url: '/disponiveis',
    views: {
      'tab-disponiveis': {
        templateUrl: 'templates/tab-disponiveis.html',
        controller: 'disponiveisCtrl'
      }
    }
  })

  .state('tab.perfil', {
    url: '/perfil',
    views: {
      'tab-perfil': {
        templateUrl: 'templates/tab-perfil.html',
        controller: 'perfilCtrl'
      }
    }
  })

  .state('login', {
    url: '/login',
    templateUrl: 'templates/login.html',
    controller: 'loginCtrl'
  })

  .state('criar-usuario', {
    url: '/criar-usuario',
    templateUrl: 'templates/criar-usuario.html',
    controller: 'criarUsuarioCtrl'
  });

  // if none of the above states are matched, use this as the fallback
  $urlRouterProvider.otherwise('/login');

  $httpProvider.interceptors.push('LoadingInterceptor');
})

.directive('fancySelect', ['$ionicModal', function($ionicModal) {
  return {
    /* Only use as <fancy-select> tag */
    restrict : 'E',

    /* Our template */
    templateUrl: 'templates/directives/fancy-select.html',

    /* Attributes to set */
    scope: {
        'items'        : '=', /* Items list is mandatory */
        'text'         : '=', /* Displayed text is mandatory */
        'value'        : '=', /* Selected value binding is mandatory */
        'callback'     : '&',
    },

    link: function (scope, element, attrs) {

      /* Default values */
      scope.multiSelect   = attrs.multiSelect === 'true' ? true : false;
      scope.allowEmpty    = attrs.allowEmpty === 'false' ? false : true;

      /* Header used in ion-header-bar */
      scope.headerText    = attrs.headerText || '';

      /* Text displayed on label */
      // scope.text          = attrs.text || '';
      scope.defaultText   = attrs.text || '';

      /* Notes in the right side of the label */
      scope.noteText      = attrs.noteText || '';
      scope.noteImg       = attrs.noteImg || '';
      scope.noteImgClass  = attrs.noteImgClass || '';
        
      /* Optionnal callback function */
      // scope.callback = attrs.callback || null;

      /* Instanciate ionic modal view and set params */

      /* Some additionnal notes here : 
       * 
       * In previous version of the directive,
       * we were using attrs.parentSelector
       * to open the modal box within a selector. 
       * 
       * This is handy in particular when opening
       * the "fancy select" from the right pane of
       * a side view. 
       * 
       * But the problem is that I had to edit ionic.bundle.js
       * and the modal component each time ionic team
       * make an update of the FW.
       * 
       * Also, seems that animations do not work 
       * anymore.
       * 
       */
      $ionicModal.fromTemplateUrl(
        'templates/directives/fancy-select-items.html',
          {'scope': scope}
      ).then(function(modal) {
        scope.modal = modal;
      });

      /* Validate selection from header bar */
      scope.validate = function (event) {
        // Construct selected values and selected text
        if (scope.multiSelect == true) {

          // Clear values
          scope.value = '';
          scope.text = '';

          // Loop on items
          jQuery.each(scope.items, function (index, item) {
            if (item.checked) {
              scope.value = scope.value + item.id+';';
              scope.text = scope.text + item.texto+', ';
            }
          });

          // Remove trailing comma
          scope.value = scope.value.substr(0,scope.value.length - 1);
          scope.text = scope.text.substr(0,scope.text.length - 2);
        }

        // Select first value if not nullable
        if (typeof scope.value == 'undefined' || scope.value == '' || scope.value == null ) {
          if (scope.allowEmpty == false) {
            scope.value = scope.items[0].id;
            scope.text = scope.items[0].text;

            // Check for multi select
            scope.items[0].checked = true;
          } else {
            scope.text = scope.defaultText;
          }
        }

        // Hide modal
        scope.hideItems();
        
        // Execute callback function
        if (typeof scope.callback == 'function') {
          scope.callback (scope.value);
        }
      }

      /* Show list */
      scope.showItems = function (event) {
        event.preventDefault();
        scope.modal.show();
      }

      /* Hide list */
      scope.hideItems = function () {
        scope.modal.hide();
      }

      /* Destroy modal */
      scope.$on('$destroy', function() {
        scope.modal.remove();
      });

      /* Validate single with data */
      scope.validateSingle = function (item) {
        // Set selected text
        scope.text = item.texto;

        // Set selected value
        scope.value = item.id;

        // Hide items
        scope.hideItems();
        
        // Execute callback function
        if (typeof scope.callback == 'function') {
            scope.callback (scope.value);
        }
      }
    }
  };
}]);
